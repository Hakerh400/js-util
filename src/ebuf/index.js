'use strict';

const empty = Buffer.alloc(1);

class ExpandableBuffer{
  len = 0
  buf = Buffer.from(empty)
  
  constructor(data = null){
    if(data !== null){
      let buf = Buffer.from(data)
      
      for(let b of buf)
        this.push(b)
    }
  }

  reset(){
    this.buf = Buffer.from(empty);
    this.len = 0;
  }

  push(byte){
    if(this.len === this.buf.length)
      this.buf = Buffer.concat([this.buf, this.buf]);
    this.buf[this.len++] = byte;
    return this;
  }

  getBuf(){
    return Buffer.from(this.buf.slice(0, this.len));
  }
}

module.exports = ExpandableBuffer;